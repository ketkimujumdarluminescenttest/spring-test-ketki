package com.lumidigital.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lumidigital.model.Product;
import com.lumidigital.service.ProductService;


@Controller
public class ProductListController {
	
	public static final String REDIRECT_PREFIX = "redirect:";
	
	@Autowired
	private ProductService productService;	
	
	@RequestMapping(value = "/product/list", method = RequestMethod.GET)
	public String getProductList(Model model, HttpServletRequest request) {
		
		Iterable<Product> productList = productService.getProducts();
		
		model.addAttribute("productList", productList);
		
		HttpSession session = request.getSession();
		
		String sessionCurrency = (String) session.getAttribute("sessionCurrency");
		if(StringUtils.isEmpty(sessionCurrency)) {
			session.setAttribute("sessionCurrency", "GBP");
		}
		return "productListPage";
	}
	
	@RequestMapping(value = "/updateCurrency", method = {RequestMethod.GET, RequestMethod.POST })
	public String updateCurrency(Model model, HttpServletRequest request, @RequestParam("currency") final String currency) {
		
		HttpSession session = request.getSession();
		session.setAttribute("sessionCurrency", currency);
		
		return REDIRECT_PREFIX + "/product/list";
	}
	
	@ResponseBody
	@RequestMapping(value = "/product/list", method = RequestMethod.GET, produces = "application/json")
	public Iterable<Product> getProductListasJSON(Model model, HttpServletRequest request) {
		
		Iterable<Product> productList = productService.getProducts();
		return productList;
	}
	
}
