package com.lumidigital.controllers;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lumidigital.form.EditProductForm;
import com.lumidigital.model.Price;
import com.lumidigital.model.Product;
import com.lumidigital.service.ProductService;

@Controller
public class EditProductController {
	
	public static final String REDIRECT_PREFIX = "redirect:";

	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/editProduct", method = RequestMethod.GET)
	String editProduct(Model model, HttpServletRequest request, @RequestParam("productId") final Long productId) {
		
		Product product = productService.getProductForId(productId);
		
		model.addAttribute("product", product);
		
		model.addAttribute("editProductForm", new EditProductForm());
		
		return "editProductPage";
	}
	
	@RequestMapping(value = "/submitProductChanges", method = RequestMethod.POST)
	String saveProductChanges(@Valid @ModelAttribute("editProductForm") final EditProductForm editProductForm, BindingResult bindingResult, Model model)
	{
		if (bindingResult.hasErrors()) {
			model.addAttribute("editProductForm", editProductForm);
			
			return "editProductPage";
		}
		else {
			Product updatedProduct = productService.getProductForId(editProductForm.getId());
			
			updatedProduct.setName(editProductForm.getProductName());
			
			Currency currency = Currency.getInstance(editProductForm.getCurrency());
			
			Map<Currency, Price> existingPrices = updatedProduct.getPrices();
			
			for (Map.Entry<Currency, Price> price : existingPrices.entrySet()) {
				if(price.getKey().equals(currency))
				{
					Price existingPrice = price.getValue();
					existingPrice.setAmount(BigDecimal.valueOf(Double.valueOf(editProductForm.getProductPrice())));
					existingPrices.put(currency, existingPrice);
				}
			}
			
			updatedProduct.setPrices(existingPrices);
			productService.save(updatedProduct);
			
			return REDIRECT_PREFIX + "/product/list";
		}
		
		
	}
}
