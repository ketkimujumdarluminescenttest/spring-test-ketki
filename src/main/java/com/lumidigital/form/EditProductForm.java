package com.lumidigital.form;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class EditProductForm {
	
	private Long id;
	
	@NotEmpty(message = "Product Name can not be empty")
    @Size(max=50, message = "Product Name can not be longer than 50 characters")
	private String productName;
	
	@NotEmpty(message = "Product price can not empty")
	@DecimalMin("0.01") 
	private String productPrice;
	
	@NotEmpty(message = "Currency can not empty")
	private String currency;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}
