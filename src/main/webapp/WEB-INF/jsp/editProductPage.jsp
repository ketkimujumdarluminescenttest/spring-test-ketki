<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Edit Product Page</title>
	<style>
		.error {
			color: red;
		}						
	</style>
	</head>
	<body>
		<form:form id="editProductForm" action="/submitProductChanges" method="post" modelAttribute="editProductForm">
			<form:input type="hidden" value="${product.id}" path="id"/>
			Product Name: <form:input path="productName" value="${product.name}"/>
			<br/>
			<form:errors path="productName" cssClass="error" />
			<br/><br/>
			Product Price: <form:input path="productPrice"/>
			<br/>
			<form:errors path="productPrice" cssClass="error" />
			<br/><br/>
			Currency: 
			<form:select path="currency">
				<option></option>
		    	<option value="GBP" ${editProductForm.currency == 'GBP' ? 'selected="selected"' : ''}>GBP</option>
		   		<option value="EUR" ${editProductForm.currency == 'EUR' ? 'selected="selected"' : ''}>EUR</option>
		  	</form:select>
		  	<br/>
		  	<form:errors path="currency" cssClass="error"/>
		  	<br/><br/>
		  	<input type="submit">
	  	</form:form>
	</body>
</html>