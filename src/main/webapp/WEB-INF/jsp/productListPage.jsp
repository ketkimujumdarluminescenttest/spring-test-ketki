<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set value="${sessionScope.sessionCurrency}" var="sessionCurrency"/>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product List Page</title>
<style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
}
</style>
</head>
<body>
	<span>Please choose your Currency</span>	
	
	
	<form action="/updateCurrency" id="currencyForm" method="post">
		<select name="currency">
			<option value="GBP" ${sessionCurrency == 'GBP' ? 'selected="selected"' : ''}>GBP</option>
			<option value="EUR" ${sessionCurrency == 'EUR' ? 'selected="selected"' : ''}>EUR</option>
		</select>
  		<input type="submit">
	</form> 
	
	<br/><br/>
	<table class="responsive-table" id="productListTable">
		<thead class="responsive-table-head hidden-xs">
			<tr>
			    <th>Product Name</th>
			    <th>Price</th>
			    <th></th>
	  		</tr>
		</thead>
		<tbody>
  			<c:forEach var="product" items="${productList}">
		 		<tr class="responsive-table-item">
				    <td>${product.name}</td>
				    <c:forEach var="price" items="${product.prices}">
				    	<c:if test="${(price.key.currencyCode).equalsIgnoreCase(sessionCurrency)}">
				    		<td>${price.value.amount}  &nbsp; ${price.key.currencyCode}</td>
				    	</c:if>
				    </c:forEach>
				    <td><a href="/editProduct?productId=${product.id}">Edit</a></td>
			  	</tr>
	  		</c:forEach>
	  	</tbody>
	</table>
</body>
</html>